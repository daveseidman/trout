import './style.scss';

import { TrackballControls } from './threejs/TrackballControls';

const THREE = require('three');

let wrap;
let input;
let output;
let scene;
let camera;
let renderer;
let container;
let globe;
let points;
let controls;

const pointsArray = [];

const radius = 100;


const latLonToPos = (lat, lon) => {
  const phi = (90 - lat) * (Math.PI / 180);
  const theta = (lon + 180) * (Math.PI / 180);

  const x = -((radius) * Math.sin(phi) * Math.cos(theta));
  const y = ((radius) * Math.cos(phi));
  const z = ((radius) * Math.sin(phi) * Math.sin(theta));

  return new THREE.Vector3(x, y, z);
};


const poll = (id) => {
  fetch('/api/status', {
    method: 'post',
    body: JSON.stringify({ id }),
    headers: { 'Content-type': 'application/json' },
  }).then(response => response.json()).then((data) => {
    output.innerHTML = '<span class=\'data\'><b>IP</b></span>'
              + '<span class=\'data\'><b>Lat</b></span>'
              + '<span class=\'data\'><b>Lon</b></span>';

    for (let p = 0; p < pointsArray.length; p += 1) {
      const point = pointsArray[p];
      points.remove(point);
    }


    for (let i = 1; i < data.ipList.length; i += 1) {
      const { address, latitude, longitude } = data.ipList[i];
      const el = document.createElement('li');
      el.innerHTML = `<span class='data'>${address}</span>`
                + `<span class='data'>${latitude}</span>`
                + `<span class='data'>${longitude}</span>`;

      output.appendChild(el);

      if (latitude && longitude) {
        const position = latLonToPos(latitude, longitude);
        const point = new THREE.Mesh(
          new THREE.SphereGeometry(3, 16, 16),
          new THREE.MeshPhongMaterial({ color: 0xff0000 }),
        );
        point.position.x = position.x;
        point.position.y = position.y;
        point.position.z = position.z;

        pointsArray.push(point);
        points.add(point);
      }
    }

    if (data.status === 'complete') {
      console.log('all done');
    } else {
      setTimeout(poll, 1000, id);
    }
  });
};


const keydown = (e) => {
  if (e.which === 13) {
    // input.setAttribute('disabled', true);
    console.log('sending', input.value);
    fetch('/api/site', {
      method: 'post',
      body: JSON.stringify({ site: input.value }),
      headers: { 'Content-Type': 'application/json' },
    }).then(response => response.json()).then((data) => {
      poll(data.id);
    });
  }
};

const setupUI = () => {
  wrap = document.createElement('wrap');
  wrap.className = 'wrap';

  const label = document.createElement('p');
  label.innerText = 'input website:';
  wrap.appendChild(label);

  input = document.createElement('input');
  input.className = 'input';
  input.setAttribute('type', 'text');
  input.setAttribute('name', 'site');
  input.addEventListener('keydown', keydown);
  wrap.appendChild(input);

  output = document.createElement('ol');
  output.className = 'output';
  wrap.appendChild(output);

  document.body.appendChild(wrap);
  input.focus();
};

const animate = () => {
  renderer.render(scene, camera);
  controls.update();
  requestAnimationFrame(animate);
};


const setupScene = () => {
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 1, 1000);
  camera.position.set(0, 0, 500);
  controls = new THREE.TrackballControls(camera);
  controls.noZoom = true;
  controls.noPan = true;
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.domElement.className = 'threejs';
  document.body.appendChild(renderer.domElement);

  const geometry = new THREE.SphereGeometry(radius, 64, 64);
  const texture = new THREE.TextureLoader().load('img/earth.jpg');
  const material = new THREE.MeshPhongMaterial({ map: texture });

  points = new THREE.Object3D();
  globe = new THREE.Mesh(geometry, material);
  container = new THREE.Object3D();
  container.add(points);
  container.add(globe);
  scene.add(container);

  const ambientLight = new THREE.AmbientLight(0xffffff, 0.3);
  scene.add(ambientLight);

  const pointLight = new THREE.PointLight(0xffffff, 1.8, 2000);
  pointLight.position.set(200, 800, 800);
  scene.add(pointLight);

  animate();
};

setupScene();
setupUI();
