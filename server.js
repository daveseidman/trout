const { spawn } = require('child_process');
const ipapi = require('ipapi.co');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const PORT = 8000;

const requests = [];

const parseIP = (string) => {
  const start = string.indexOf('(') + 1;
  const end = string.indexOf(')');
  const ip = {
    address: string.substring(start, end),
  };
  return ip;
};


const attachLocToIP = (ip) => {
  ipapi.location((res) => {
    ip.latitude = res.latitude;
    ip.longitude = res.longitude;
  }, ip.address);
};

const runTrace = (response) => {
  console.log('running trace', response);
  return new Promise((resolve) => {
    const { site } = response;
    response.ipList = [];
    const tr = spawn('traceroute', [site]);
    tr.stdout.on('data', (data) => {
      const string = data.toString();
      if (string.indexOf('(') >= 0 && string.indexOf(')')) {
        const ip = parseIP(string);
        attachLocToIP(ip);
        response.ipList.push(ip);
      }
    });

    tr.on('exit', () => {
      response.status = 'complete';
      resolve(response.ipList);
    });
  });
};


app.use(express.static('dist'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/api/site', (req, res) => {
  const id = Math.random().toString(36).substring(2, 15);
  const request = {
    id,
    site: req.body.site,
    status: 'started',
  };
  requests.push(request);

  runTrace(request).then((response) => {
    console.log(response);
  });
  res.send({ id });
});

app.post('/api/status', (req, res) => {
  for (let i = 0; i < requests.length; i += 1) {
    const request = requests[i];
    if (request.id === req.body.id) {
      res.send(request);
    }
  }
});

app.listen(PORT, () => { console.log('app listening on port', PORT); });
